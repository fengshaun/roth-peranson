use std::vec::Vec;
use std::collections::HashMap;
use serde_derive::{Serialize, Deserialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct Applicant {
    pub id: i32,
    pub name: String,
    pub pending: bool,
    pub rol: Vec<ProgramRank>,
    pub matched: Option<ProgramRank>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Program {
    pub id: i32,
    pub name: String,
    pub matches: Vec<ApplicantRank>,
    pub rol: Vec<ApplicantRank>,
    pub capacity: usize,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ApplicantRank {
    pub rank: usize,
    pub id: i32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ProgramRank {
    pub rank: usize,
    pub id: i32,
}

pub fn run_match_algo(
    programs: &mut HashMap<i32, Program>,
    applicants: &mut HashMap<i32, Applicant>,
    output: &mut Vec<String>)
{
    loop {
        let pending: Vec<i32> = applicants.keys()
            .filter(|k| if let Some(x) = applicants.get(&k) {
                x.pending
            } else {
                false
            })
            .map(|x| *x)
            .collect();

        if pending.len() == 0 {
            break;
        }

        for applicant_id in pending {
            output.push(format!("attempting to match applicant {}", applicant_id));
            match try_match_applicant(&applicants[&applicant_id], programs, output) {
                (Some(p_rank), Some(aid)) => {
                    output.push(format!("applicant {} matched, displacing and rejecting {}",
                                        applicant_id,
                                        aid
                    ));

                    if let Some(applicant) = applicants.get_mut(&applicant_id) {
                        applicant.matched = Some(p_rank);
                    }

                    // put rejected applicant back into the pool
                    if let Some(applicant) = applicants.get_mut(&aid) {
                        applicant.pending = true;
                        applicant.matched = None;
                    }
                },

                (Some(p_rank), _) => {
                    output.push(format!("applicant {} matched, removing from pending list",
                            applicant_id
                    ));

                    if let Some(applicant) = applicants.get_mut(&applicant_id) {
                        applicant.matched = Some(p_rank);
                    }
                },

                _ => {
                    output.push(format!("applicant {} did not match, removing from pending list",
                            applicant_id
                    ));
                },
            }

            // we've taken care of this applicant
            if let Some(applicant) = applicants.get_mut(&applicant_id) {
                applicant.pending = false;
            }
        }
    }
}

fn try_match_applicant(
    applicant: &Applicant,
    progs: &mut HashMap<i32, Program>,
    output: &mut Vec<String>)
    -> (Option<ProgramRank>, Option<i32>)
{
    // returns the program id that matched the applicant and the applicant id that got rejected
    // as a result of this match

    for p_rank in applicant.rol.iter() {
        // find the program
        let prog: &mut Program = match progs.values_mut().find(|p| p.id == p_rank.id) {
            Some(p) => p,
            None => {
                output.push(format!("program {} not found", p_rank.id));
                continue;
            },
        };

        // find program's ranking of the applicant
        let rank: usize = match prog.rol.iter().find(|a_rank| a_rank.id == applicant.id) {
            Some(r) => r.rank,
            None => {
                output.push(format!("program {} did not rank applicant", prog.id));
                continue;
            },
        };

        // the program has empty spots
        if prog.matches.len() < prog.capacity {
            output.push(format!("program {} has an empty seat and has ranked applicant {}", prog.id, applicant.id));
            prog.matches.push(ApplicantRank {rank: rank, id: applicant.id});
            return (Some(p_rank.clone()), None);
        }

        // find lower ranked (higher index) applicants than the applicant we're trying to match
        let (index, lower_rank): (usize, ApplicantRank) = match prog.matches.iter()
            .enumerate()
            .find(|(_, a_rank)| a_rank.rank > rank)
        {
            Some((i, lower_rank)) => (i, (*lower_rank).clone()),
            
            // the program already has its preferred applicants
            None => {
                output.push(format!("program {} already has its preferred applicants", prog.id));
                continue;
            },
        };
        
        output.push(format!("program {} prefers {} instead of {}", prog.id, applicant.id, lower_rank.id));
        prog.matches.remove(index);
        prog.matches.push(ApplicantRank {rank: rank, id: applicant.id});
        return (Some(p_rank.clone()), Some(lower_rank.id));
    }

    // 404 match not found
    (None, None)
}

pub fn ensure_match_stability(
    programs: &HashMap<i32, Program>,
    applicants: &HashMap<i32, Applicant>,
    output: &mut Vec<String>)
    -> bool
{

    /*
    definition for stable: there is no match where
    applicant A prefers program P where P ALSO prefers A to its own matches (AND)

    for every applicant A(m) who is matched to some program P(m) (matched)
    ....for every potential program P(p) with its own matched applicant A(p,m)
    ........unstable if P(p) has ranked A(m) higher than any A(p,m)
    match is considered stable if it is not unstable
     */

    for applicant in applicants.values().filter(|a| a.matched.is_some()) {
        let matched_applicant_rank = match applicant.matched.as_ref() {
            Some(r) => r,
            None => panic!("Fatal error: cannot retrieve a matched applicant's match status"),
        };

        output.push(format!("Considering applicant {} who matched to program {}", applicant.id, matched_applicant_rank.id));

        let matched_program_rank =
            programs
            .get(&matched_applicant_rank.id)
            .map(|prog| prog.rol.iter().find(|x| x.id == applicant.id))
            .expect("Applicants can only match to programs that have ranked them")
            .expect("Applicants can only match to programs within the matching system");

        for ranked_program in applicant.rol.iter().filter(|r| r.rank < matched_applicant_rank.rank) {

            let prog = match programs.get(&ranked_program.id) {
                Some(p) => p,
                None => {
                    output.push(format!("program {} does not exist", ranked_program.id));
                    continue;
                },
            };


            output.push(format!("Considering program {} for applicant {}", prog.id, applicant.id));
            let other_program_rank = match prog.rol.iter().find(|r| r.id == applicant.id) {
                Some(x) => x,
                None => {
                    output.push(format!("program {} has not ranked applicant {}", prog.id, applicant.id));
                    continue
                }
            };

            if other_program_rank.rank < matched_program_rank.rank {
                output.push(format!("match is unstable: program {} and applicant {} make a more stable match", other_program_rank.id, applicant.id));
                return false
            } else {
                output.push(format!("program {} has ranked applicant {} lower than its own current matches", other_program_rank.id, applicant.id));
            }
        }

        output.push(format!("exhausted all programs which were ranked higher by applicant {} than currently matched program", applicant.id));
    }

    true
}

pub fn make_rol_applicant(v: &[i32]) -> Vec<ApplicantRank> {
    return v
        .into_iter()
        .zip(0..)
        .map(|(id, rank)| ApplicantRank { rank, id: *id })
        .collect();
}

pub fn make_rol_program(v: &[i32]) -> Vec<ProgramRank> {
    return v
        .into_iter()
        .zip(0..)
        .map(|(id, rank)| ProgramRank { rank, id: *id })
        .collect();
}

#[cfg(test)]
mod tests {
    use std::vec::Vec;
    use std::collections::HashMap;
    use crate::*;


    fn check_matched(p: &Program, a: &Applicant) -> bool {
        match p.matches.iter().find(|r| r.id == a.id) {
            Some(_) => return true,
            None => return false,
        }
    }

    fn check_unmatched(ps: &HashMap<i32, Program>, a: &Applicant) -> bool {
        for p in ps.values() {
            for m in p.matches.iter() {
                if m.id == a.id {
                    return false;
                }
            }
        }

        return true;
    }


    #[test]
    fn test_official_carms_case() {
        let mut applicants: HashMap<i32, Applicant> = vec![
            ("Anderson", make_rol_program(&[1])),
            ("Beaudry", make_rol_program(&[1, 0])),
            ("Chen", make_rol_program(&[1, 0])),
            ("Davis", make_rol_program(&[0, 1, 2, 3])),
            ("Eastman", make_rol_program(&[1, 0, 3, 2])),
            ("Feldman", make_rol_program(&[1, 2, 0, 3])),
            ("Garcia", make_rol_program(&[1, 0, 3, 2])),
            ("Hassan", make_rol_program(&[3, 1, 0, 2]))
        ].iter()
            .enumerate()
            .map(|(i, (name, rol))|
                 (i as i32, Applicant {
                     id: i as i32,
                     name: name.to_string(),
                     rol: rol.to_vec(),
                     pending: true,
                     matched: None,
                 }))
            .collect();

        let mut programs: HashMap<i32, Program> = vec![
            ("Mercy", make_rol_applicant(&[2, 6])),
            ("City", make_rol_applicant(&[6, 7, 4, 0, 1, 2, 3, 5])),
            ("General", make_rol_applicant(&[1, 4, 7, 0, 2, 3, 6])),
            ("State", make_rol_applicant(&[1, 4, 0, 2, 7, 5, 3, 6])),
        ].iter()
            .enumerate()
            .map(|(i, (name, rol))|
                 (i as i32, Program {
                     id: i as i32,
                     name: name.to_string(),
                     rol: rol.to_vec(),
                     capacity: 2,
                     matches: Vec::with_capacity(2),
                 }))
            .collect();

        let mut output = Vec::<String>::new();
        run_match_algo(&mut programs, &mut applicants, &mut output);

        assert_eq!(check_matched(&programs[&0],  &applicants[&2]), true,
                   "Chen matched to Mercy");
        assert_eq!(check_matched(&programs[&2],  &applicants[&3]), true,
                   "Davis matched to General");
        assert_eq!(check_matched(&programs[&1],  &applicants[&4]), true,
                   "Eastman matched to City");
        assert_eq!(check_matched(&programs[&3],  &applicants[&5]), true,
                   "Feldman matched to State");
        assert_eq!(check_matched(&programs[&1],  &applicants[&6]), true,
                   "Garcia matched to City");
        assert_eq!(check_matched(&programs[&3],  &applicants[&7]), true,
                   "Hassan matched to State");

        assert_eq!(check_unmatched(&programs,&applicants[&0]), true,
                   "Anderson is unmatched");
        assert_eq!(check_unmatched(&programs,&applicants[&1]), true,
                   "Beaudry is unmatched");

        assert_eq!(ensure_match_stability(&programs, &applicants, &mut output), true,
                   "Match is stable");


        for x in output.iter() {
            println!("{}", x);
        }

        for p in programs.values() {
            for m in p.matches.iter() {
                println!("program {} matched applicant {}", p.id, m.id);
            }
        }
    }
}
