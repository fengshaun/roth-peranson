An interactive implementation of the Roth Peranson algorithm used in CaRMS and NRMP matching systems.
See [the published paper](https://web.stanford.edu/~alroth/papers/rothperansonaer.PDF) for implementation details.
